Simple test
------------

Ensure your device works with this simple test.

.. literalinclude:: ../examples/framework_simpletest.py
    :caption: examples/framework_simpletest.py
    :linenos:
