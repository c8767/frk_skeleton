# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT
"""
`frk_skeleton`
================================================================================

.. todo:: Write something meaningful regarding your peripheral


* Author(s): Nathan Woody

Implementation Notes
--------------------

**Software and Dependencies:**

* Adafruit CircuitPython firmware >=7.1.0 for the supported boards:
  https://circuitpython.org/downloads

* asyncio

* adafruit_ticks

* framework for CircuitPython

"""

# imports
import board
import sys
import asyncio

__version__ = "0.0.0-auto.0"

#imports
import asyncio