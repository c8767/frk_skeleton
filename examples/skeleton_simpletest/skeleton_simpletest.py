# SPDX-FileCopyrightText: Copyright (c) 2022 Nathan Woody
#
# SPDX-License-Identifier: MIT
"""
`skeleton_simpletest`
================================================================================

.. todo:: Write something meaningful regarding your example


* Author(s): Nathan Woody

Implementation Notes
--------------------

**Software and Dependencies:**

* Adafruit CircuitPython firmware >=7.1.0 for the supported boards:
  https://circuitpython.org/downloads

* asyncio

* adafruit_ticks

"""

# imports
import board
import sys
import asyncio

__version__ = "0.0.0-auto.0"

#imports
import asyncio